import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { applyMiddleware, compose, createStore } from "redux";
import createSagaMiddleware from "redux-saga";
import App from "./App";
import { consoleMiddleware, emptyMiddleware, logger } from "./middlewares";
import appReducer from "./reducers";
import rootSaga from "./sagas";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  appReducer,
  compose(
    applyMiddleware(
      consoleMiddleware(),
      emptyMiddleware,
      logger(),
      sagaMiddleware
    ),
    process.env.NODE_ENV === "development" && (window as any).devToolsExtension
      ? (window as any).devToolsExtension()
      : (f: any) => f
  )
);

sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root") as HTMLElement
);
