import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import ButtonLink from "../components/ButtonLink";
import RsvAppShell from "../components/RsvAppShell";

const gridSpacing = 16;

const RsvHome: React.SFC<RouteComponentProps> = ({ location }) => {
  return (
    <>
      <Helmet>
        <title>トップ</title>
      </Helmet>

      <RsvAppShell title="トップ" pathname={location.pathname}>
        <div style={{ padding: 16 }}>
          <Grid container={true} spacing={gridSpacing} style={{ marginTop: 8 }}>
            <Grid item={true} xs={12}>
              <Card>
                <CardContent>
                  <Typography variant="h5" component="h3">
                    予約
                  </Typography>

                  <Typography component="p" style={{ marginTop: 8 }}>
                    いつ、どのユーザーが、どのメニューを利用するかという情報。
                  </Typography>
                </CardContent>

                <CardActions>
                  <ButtonLink to="/rsv-reservations/" size="small">
                    一覧へ
                  </ButtonLink>
                </CardActions>
              </Card>
            </Grid>
          </Grid>

          <Grid container={true} spacing={gridSpacing} style={{ marginTop: 8 }}>
            <Grid item={true} xs={12} sm={4}>
              <Card>
                <CardContent>
                  <Typography variant="h5" component="h3">
                    店舗
                  </Typography>
                </CardContent>

                <CardActions>
                  <ButtonLink to="/rsv-shops/" size="small">
                    一覧へ
                  </ButtonLink>
                </CardActions>
              </Card>
            </Grid>

            <Grid item={true} xs={12} sm={4}>
              <Card>
                <CardContent>
                  <Typography variant="h5" component="h3">
                    メニュー
                  </Typography>
                </CardContent>

                <CardActions>
                  <ButtonLink to="/rsv-menus/" size="small">
                    一覧へ
                  </ButtonLink>
                </CardActions>
              </Card>
            </Grid>

            <Grid item={true} xs={12} sm={4}>
              <Card>
                <CardContent>
                  <Typography variant="h5" component="h3">
                    ユーザー
                  </Typography>
                </CardContent>

                <CardActions>
                  <ButtonLink to="/rsv-users/" size="small">
                    一覧へ
                  </ButtonLink>
                </CardActions>
              </Card>
            </Grid>
          </Grid>
        </div>
      </RsvAppShell>
    </>
  );
};

export default withRouter(RsvHome);
