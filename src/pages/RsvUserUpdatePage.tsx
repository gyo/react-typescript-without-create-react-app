import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import RsvAppShell from "../components/RsvAppShell/";
import RsvUserForm from "../containers/RsvUserForm/Updater";

export type IRsvUserPageProps = RouteComponentProps<{ userId: string }>;

const Component: React.SFC<IRsvUserPageProps> = ({
  history,
  match,
  location
}) => {
  const userId = match.params.userId;

  return (
    <>
      <Helmet>
        <title>ユーザー編集</title>
      </Helmet>

      <RsvAppShell title="ユーザー編集" pathname={location.pathname}>
        <RsvUserForm
          userId={userId}
          history={history}
          transitionTo={`/rsv-users/${userId}/`}
          submitText="保存する"
        />
      </RsvAppShell>
    </>
  );
};

export default withRouter(Component);
