import AddIcon from "@material-ui/icons/Add";
import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import FabLink from "../components/FabLink";
import RsvAppShell from "../components/RsvAppShell/";
import RsvShopList from "../containers/RsvShopList/";

const Component: React.SFC<RouteComponentProps> = ({ location }) => {
  return (
    <>
      <Helmet>
        <title>店舗一覧</title>
      </Helmet>

      <RsvAppShell title="店舗一覧" pathname={location.pathname}>
        <RsvShopList />

        <div style={{ position: "fixed", right: 16, bottom: 16 }}>
          <FabLink to="/rsv-shops/create/" aria-label="新規作成">
            <AddIcon />
          </FabLink>
        </div>
      </RsvAppShell>
    </>
  );
};

export default withRouter(Component);
