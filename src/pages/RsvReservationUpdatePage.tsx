import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import RsvAppShell from "../components/RsvAppShell/";
import RsvReservationForm from "../containers/RsvReservationForm/Updater";

export type IRsvReservationPageProps = RouteComponentProps<{
  reservationId: string;
}>;

const Component: React.SFC<IRsvReservationPageProps> = ({
  history,
  match,
  location
}) => {
  const reservationId = match.params.reservationId;

  return (
    <>
      <Helmet>
        <title>予約編集</title>
      </Helmet>

      <RsvAppShell title="予約編集" pathname={location.pathname}>
        <RsvReservationForm
          reservationId={reservationId}
          history={history}
          transitionTo={`/rsv-reservations/${reservationId}/`}
          submitText="保存する"
        />
      </RsvAppShell>
    </>
  );
};

export default withRouter(Component);
