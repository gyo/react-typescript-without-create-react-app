import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import RsvAppShell from "../components/RsvAppShell/";
import RsvReservationForm from "../containers/RsvReservationForm/Creator";

const Component: React.SFC<RouteComponentProps> = ({ history, location }) => {
  return (
    <>
      <Helmet>
        <title>予約新規作成</title>
      </Helmet>

      <RsvAppShell title="ユーザー新規作成" pathname={location.pathname}>
        <RsvReservationForm
          history={history}
          transitionTo={`/rsv-reservations/`}
          submitText="保存する"
        />
      </RsvAppShell>
    </>
  );
};

export default withRouter(Component);
