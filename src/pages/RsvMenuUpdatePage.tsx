import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import RsvAppShell from "../components/RsvAppShell/";
import RsvMenuForm from "../containers/RsvMenuForm/Updater";

export type IRsvMenuPageProps = RouteComponentProps<{ menuId: string }>;

const Component: React.SFC<IRsvMenuPageProps> = ({
  history,
  match,
  location
}) => {
  const menuId = match.params.menuId;
  return (
    <>
      <Helmet>
        <title>メニュー</title>
      </Helmet>

      <RsvAppShell title="メニュー編集" pathname={location.pathname}>
        <RsvMenuForm
          menuId={menuId}
          history={history}
          transitionTo={`/rsv-menus/${menuId}/`}
          submitText="保存する"
        />
      </RsvAppShell>
    </>
  );
};

export default withRouter(Component);
