import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import RsvAppShell from "../components/RsvAppShell/";
import RsvMenuForm from "../containers/RsvMenuForm/Creator";

const Component: React.SFC<RouteComponentProps> = ({ history }) => {
  return (
    <>
      <Helmet>
        <title>メニュー新規作成</title>
      </Helmet>

      <RsvAppShell title="メニュー新規作成" pathname={location.pathname}>
        <RsvMenuForm
          history={history}
          transitionTo={`/rsv-menus/`}
          submitText="保存する"
        />
      </RsvAppShell>
    </>
  );
};

export default withRouter(Component);
