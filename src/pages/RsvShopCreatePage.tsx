import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import RsvAppShell from "../components/RsvAppShell/";
import RsvShopForm from "../containers/RsvShopForm/Creator";

const Component: React.SFC<RouteComponentProps> = ({ history }) => {
  return (
    <>
      <Helmet>
        <title>店舗新規作成</title>
      </Helmet>

      <RsvAppShell title="店舗新規作成" pathname={location.pathname}>
        <RsvShopForm
          history={history}
          transitionTo={`/rsv-shops/`}
          submitText="保存する"
        />
      </RsvAppShell>
    </>
  );
};

export default withRouter(Component);
