import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import RsvAppShell from "../components/RsvAppShell/";
import RsvShopForm from "../containers/RsvShopForm/Updater";

export type IRsvShopPageProps = RouteComponentProps<{ shopId: string }>;

const Component: React.SFC<IRsvShopPageProps> = ({
  history,
  match,
  location
}) => {
  const shopId = match.params.shopId;

  return (
    <>
      <Helmet>
        <title>店舗編集</title>
      </Helmet>

      <RsvAppShell title="店舗編集" pathname={location.pathname}>
        <RsvShopForm
          shopId={shopId}
          history={history}
          transitionTo={`/rsv-shops/${shopId}/`}
          submitText="保存する"
        />
      </RsvAppShell>
    </>
  );
};

export default withRouter(Component);
