import EditIcon from "@material-ui/icons/Edit";
import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import FabLink from "../components/FabLink";
import RsvAppShell from "../components/RsvAppShell/";
import RsvMenuForm from "../containers/RsvMenuForm/Reader";

export type IRsvMenuPageProps = RouteComponentProps<{ menuId: string }>;

const Component: React.SFC<IRsvMenuPageProps> = ({ match, location }) => {
  const menuId = match.params.menuId;
  return (
    <>
      <Helmet>
        <title>メニュー</title>
      </Helmet>

      <RsvAppShell title="メニュー" pathname={location.pathname}>
        <RsvMenuForm mode="viewer" menuId={menuId} />

        <div style={{ position: "fixed", right: 16, bottom: 16 }}>
          <FabLink to={`/rsv-menus/${menuId}/update/`} aria-label="編集">
            <EditIcon />
          </FabLink>
        </div>
      </RsvAppShell>
    </>
  );
};

export default withRouter(Component);
