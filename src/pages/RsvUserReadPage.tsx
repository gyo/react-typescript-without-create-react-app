import EditIcon from "@material-ui/icons/Edit";
import * as React from "react";
import Helmet from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import FabLink from "../components/FabLink";
import RsvAppShell from "../components/RsvAppShell/";
import RsvUserForm from "../containers/RsvUserForm/Reader";

export type IRsvUserPageProps = RouteComponentProps<{ userId: string }>;

const Component: React.SFC<IRsvUserPageProps> = ({ match, location }) => {
  const userId = match.params.userId;

  return (
    <>
      <Helmet>
        <title>ユーザー</title>
      </Helmet>

      <RsvAppShell title="ユーザー" pathname={location.pathname}>
        <RsvUserForm mode="viewer" userId={userId} />

        <div style={{ position: "fixed", right: 16, bottom: 16 }}>
          <FabLink to={`/rsv-users/${userId}/update/`} aria-label="編集">
            <EditIcon />
          </FabLink>
        </div>
      </RsvAppShell>
    </>
  );
};

export default withRouter(Component);
