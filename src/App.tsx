import * as React from "react";
import { Redirect, Route, Switch } from "react-router";
import RsvHomePage from "./pages/RsvHomePage";
import RsvMenuCreatePage from "./pages/RsvMenuCreatePage";
import RsvMenuList from "./pages/RsvMenuListPage";
import RsvMenuReadPage from "./pages/RsvMenuReadPage";
import RsvMenuUpdatePage from "./pages/RsvMenuUpdatePage";
import RsvReservationCreatePage from "./pages/RsvReservationCreatePage";
import RsvReservationList from "./pages/RsvReservationListPage";
import RsvReservationReadPage from "./pages/RsvReservationReadPage";
import RsvReservationUpdatePage from "./pages/RsvReservationUpdatePage";
import RsvShopCreatePage from "./pages/RsvShopCreatePage";
import RsvShopList from "./pages/RsvShopListPage";
import RsvShopReadPage from "./pages/RsvShopReadPage";
import RsvShopUpdatePage from "./pages/RsvShopUpdatePage";
import RsvUserCreatePage from "./pages/RsvUserCreatePage";
import RsvUserList from "./pages/RsvUserListPage";
import RsvUserReadPage from "./pages/RsvUserReadPage";
import RsvUserUpdatePage from "./pages/RsvUserUpdatePage";

const App: React.SFC<{}> = () => (
  <div>
    <Switch>
      <Route path="/rsv-users/create/" component={RsvUserCreatePage} />
      <Route path="/rsv-users/:userId/update/" component={RsvUserUpdatePage} />
      <Route path="/rsv-users/:userId/" component={RsvUserReadPage} />
      <Route path="/rsv-users/" component={RsvUserList} />
      <Route path="/rsv-shops/create/" component={RsvShopCreatePage} />
      <Route path="/rsv-shops/:shopId/update/" component={RsvShopUpdatePage} />
      <Route path="/rsv-shops/:shopId/" component={RsvShopReadPage} />
      <Route path="/rsv-shops/" component={RsvShopList} />
      <Route
        path="/rsv-reservations/create/"
        component={RsvReservationCreatePage}
      />
      <Route
        path="/rsv-reservations/:reservationId/update/"
        component={RsvReservationUpdatePage}
      />
      <Route
        path="/rsv-reservations/:reservationId/"
        component={RsvReservationReadPage}
      />
      <Route path="/rsv-reservations/" component={RsvReservationList} />
      <Route path="/rsv-menus/create/" component={RsvMenuCreatePage} />
      <Route path="/rsv-menus/:menuId/update/" component={RsvMenuUpdatePage} />
      <Route path="/rsv-menus/:menuId/" component={RsvMenuReadPage} />
      <Route path="/rsv-menus/" component={RsvMenuList} />
      <Route path="/rsv-home/" component={RsvHomePage} />
      <Redirect to="/rsv-home/" />
    </Switch>
  </div>
);

export default App;
