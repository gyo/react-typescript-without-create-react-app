import { connect } from "react-redux";
import RsvMenuForm from "../../components/RsvMenuForm";
import { IRsvMenuFormProps } from "../../components/RsvMenuForm/Component";
import { IStore } from "../../stores";
import { IMenu } from "../../stores/";

interface IStoreProps {
  menu?: IMenu;
}

export default connect<IStoreProps, {}, IRsvMenuFormProps, IStore>(
  (state, props) => {
    const selectedMenu = state.menus.find(menu => menu.id === props.menuId);

    return {
      menu: selectedMenu
    };
  }
)(RsvMenuForm);
