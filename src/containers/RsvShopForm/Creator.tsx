import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { createShop } from "../../actions";
import RsvShopForm from "../../components/RsvShopForm";
import { IRsvShopFormProps } from "../../components/RsvShopForm/Component";
import { IShop } from "../../stores/";

interface IDispatchProps {
  submit: (shop: IShop) => void;
}

export default connect<{}, IDispatchProps, IRsvShopFormProps>(
  () => ({}),
  dispatch =>
    bindActionCreators(
      {
        submit: shop => createShop({ shop })
      },
      dispatch
    )
)(RsvShopForm);
