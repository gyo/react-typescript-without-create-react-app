import { connect } from "react-redux";
import RsvUserCollection from "../../components/RsvUserList/";
import { IRsvUserCollectionProps } from "../../components/RsvUserList/Component";
import { IStore } from "../../stores";

interface IStoreProps {
  users: Array<{
    id: string;
    name: string;
  }>;
}

export default connect<IStoreProps, {}, IRsvUserCollectionProps, IStore>(
  state => ({
    users: state.users
  })
)(RsvUserCollection);
