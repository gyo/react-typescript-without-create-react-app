import { connect } from "react-redux";
import RsvReservationCollection from "../../components/RsvReservationList/";
import { IRsvReservationCollectionProps } from "../../components/RsvReservationList/Component";
import { IStore } from "../../stores";
import { IReservation } from "../../stores/";

interface IStoreProps {
  reservations: IReservation[];
}

export default connect<IStoreProps, {}, IRsvReservationCollectionProps, IStore>(
  state => ({
    reservations: state.reservations
  })
)(RsvReservationCollection);
