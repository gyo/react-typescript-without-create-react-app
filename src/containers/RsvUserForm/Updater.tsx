import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { updateUser } from "../../actions";
import RsvUserForm from "../../components/RsvUserForm";
import { IRsvUserFormProps } from "../../components/RsvUserForm/Component";
import { IStore } from "../../stores";
import { IUser } from "../../stores/";

interface IStoreProps {
  user?: IUser;
}

interface IDispatchProps {
  submit: (user: IUser) => void;
}

export default connect<IStoreProps, IDispatchProps, IRsvUserFormProps, IStore>(
  (state, props) => {
    const selectedUser = state.users.find(user => user.id === props.userId);

    return {
      user: selectedUser
    };
  },
  dispatch =>
    bindActionCreators(
      {
        submit: user => updateUser({ user })
      },
      dispatch
    )
)(RsvUserForm);
