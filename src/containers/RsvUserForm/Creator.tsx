import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { createUser } from "../../actions";
import RsvUserForm from "../../components/RsvUserForm";
import { IRsvUserFormProps } from "../../components/RsvUserForm/Component";
import { IUser } from "../../stores/";

interface IDispatchProps {
  submit: (user: IUser) => void;
}

export default connect<{}, IDispatchProps, IRsvUserFormProps>(
  () => ({}),
  dispatch =>
    bindActionCreators(
      {
        submit: user => createUser({ user })
      },
      dispatch
    )
)(RsvUserForm);
