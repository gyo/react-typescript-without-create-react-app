import { connect } from "react-redux";
import RsvUserForm from "../../components/RsvUserForm";
import { IRsvUserFormProps } from "../../components/RsvUserForm/Component";
import { IStore } from "../../stores";
import { IUser } from "../../stores/";

interface IStoreProps {
  user?: IUser;
}

export default connect<IStoreProps, {}, IRsvUserFormProps, IStore>(
  (state, props) => {
    const selectedUser = state.users.find(user => user.id === props.userId);

    return {
      user: selectedUser
    };
  }
)(RsvUserForm);
