import { connect } from "react-redux";
import RsvMenuCollection from "../../components/RsvMenuList/";
import { IRsvMenuCollectionProps } from "../../components/RsvMenuList/Component";
import { IStore } from "../../stores";
import { IMenu } from "../../stores/";

interface IStoreProps {
  menus: IMenu[];
}

export default connect<IStoreProps, {}, IRsvMenuCollectionProps, IStore>(
  state => ({
    menus: state.menus
  })
)(RsvMenuCollection);
