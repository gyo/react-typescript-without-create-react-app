import { connect } from "react-redux";
import RsvReservationForm from "../../components/RsvReservationForm";
import { IRsvReservationFormProps } from "../../components/RsvReservationForm/Component";
import { IStore } from "../../stores";
import { IReservation } from "../../stores/";

interface IStoreProps {
  reservation?: IReservation;
}

export default connect<IStoreProps, {}, IRsvReservationFormProps, IStore>(
  (state, props) => {
    const selectedReservation = state.reservations.find(
      reservation => reservation.id === props.reservationId
    );

    return {
      reservation: selectedReservation
    };
  }
)(RsvReservationForm);
