import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { updateReservation } from "../../actions";
import RsvReservationForm from "../../components/RsvReservationForm";
import { IRsvReservationFormProps } from "../../components/RsvReservationForm/Component";
import { IStore } from "../../stores";
import { IMenu } from "../../stores/";
import { IReservation } from "../../stores/";
import { IUser } from "../../stores/";

interface IStoreProps {
  reservation?: IReservation;
  menus: IMenu[];
  users: IUser[];
}

interface IDispatchProps {
  submit: (reservation: IReservation) => void;
}

export default connect<
  IStoreProps,
  IDispatchProps,
  IRsvReservationFormProps,
  IStore
>(
  (state, props) => {
    const selectedReservation = state.reservations.find(
      reservation => reservation.id === props.reservationId
    );

    return {
      menus: state.menus,
      reservation: selectedReservation,
      users: state.users
    };
  },
  dispatch =>
    bindActionCreators(
      {
        submit: reservation => updateReservation({ reservation })
      },
      dispatch
    )
)(RsvReservationForm);
