import { connect } from "react-redux";
import RsvShopCollection from "../../components/RsvShopList/";
import { IRsvShopCollectionProps } from "../../components/RsvShopList/Component";
import { IStore } from "../../stores";

interface IStoreProps {
  shops: Array<{
    id: string;
    name: string;
  }>;
}

export default connect<IStoreProps, {}, IRsvShopCollectionProps, IStore>(
  state => ({
    shops: state.shops
  })
)(RsvShopCollection);
