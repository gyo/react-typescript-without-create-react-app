import actionCreatorFactory from "typescript-fsa";
import { IMenu, IReservation, IShop, IUser } from "../stores/";

const actionCreator = actionCreatorFactory();

export interface ICreateMenuActionPayload {
  menu: IMenu;
}
export const createMenu = actionCreator<ICreateMenuActionPayload>(
  "CREATE_MENU"
);

export interface IUpdateMenuActionPayload {
  menu: IMenu;
}
export const updateMenu = actionCreator<IUpdateMenuActionPayload>(
  "UPDATE_MENU"
);

export interface ICreateReservationActionPayload {
  reservation: IReservation;
}
export const createReservation = actionCreator<ICreateReservationActionPayload>(
  "CREATE_RESERVATION"
);

export interface IUpdateReservationActionPayload {
  reservation: IReservation;
}
export const updateReservation = actionCreator<IUpdateReservationActionPayload>(
  "UPDATE_RESERVATION"
);

export interface ICreateShopActionPayload {
  shop: IShop;
}
export const createShop = actionCreator<ICreateShopActionPayload>(
  "CREATE_SHOP"
);

export interface IUpdateShopActionPayload {
  shop: IShop;
}
export const updateShop = actionCreator<IUpdateShopActionPayload>(
  "UPDATE_SHOP"
);

export interface ICreateUserActionPayload {
  user: IUser;
}
export const createUser = actionCreator<ICreateUserActionPayload>(
  "CREATE_USER"
);

export interface IUpdateUserActionPayload {
  user: IUser;
}
export const updateUser = actionCreator<IUpdateUserActionPayload>(
  "UPDATE_USER"
);
