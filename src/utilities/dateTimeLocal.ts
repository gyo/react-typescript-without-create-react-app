import { stringPadStart } from "./index";

export const toString = (date: Date) => {
  const yearString: string = stringPadStart(`${date.getFullYear()}`, 4, "0");
  const monthString: string = stringPadStart(`${date.getMonth() + 1}`, 2, "0");
  const dateString: string = stringPadStart(`${date.getDate()}`, 2, "0");
  const hourString: string = stringPadStart(`${date.getHours()}`, 2, "0");
  const minutesString: string = stringPadStart(`${date.getMinutes()}`, 2, "0");

  return `${yearString}-${monthString}-${dateString}T${hourString}:${minutesString}`;
};

export const toDate = (dateTimeLocalString: string) => {
  const matched = /([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2})/.exec(
    dateTimeLocalString
  );

  if (matched === null) {
    throw new Error("dateTimeLocalString is not matched expected pattern.");
  }

  return new Date(
    `${matched[1]}-${matched[2]}-${matched[3]} ${matched[4]}:${matched[5]}`
  );
};
