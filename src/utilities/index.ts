export const stringPadStart = (
  str: string,
  maxLength: number,
  fillString: string
): string => {
  const prefix = new Array(maxLength).fill(fillString);
  return `${prefix}${str}`.slice(-1 * maxLength);
};
