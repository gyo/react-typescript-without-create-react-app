import { Dispatch, Middleware, MiddlewareAPI } from "redux";

export const emptyMiddleware: Middleware = () => (next: Dispatch) => action =>
  next(action);

export const consoleMiddleware = (): Middleware => {
  console.log("middleware-1");
  return () => {
    console.log("middleware-2");
    return (next: Dispatch) => {
      console.log("middleware-3");
      return action => {
        console.log("middleware-4");
        return next(action);
      };
    };
  };
};

export const logger = () => {
  const loggerMiddleware: Middleware = ({ getState }: MiddlewareAPI) => (
    next: Dispatch
  ) => action => {
    console.log("[# middleware logger] will dispatch", action);

    const returnValue = next(action);

    console.log("[# middleware logger] state after dispatch", getState());

    return returnValue;
  };

  return loggerMiddleware;
};
