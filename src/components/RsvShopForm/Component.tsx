import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { History } from "history";
import * as React from "react";
import { IShop } from "../../stores/";

export interface IRsvShopFormProps {
  shopId?: string;
  shop?: IShop;
  mode?: "viewer" | "editor";
  name?: string;
  updateName?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  submit?: (shop: IShop) => void;
  submitText?: string;
  history?: History;
  transitionTo?: string;
}

const Component: React.SFC<IRsvShopFormProps> = ({
  shopId,
  shop,
  mode = "editor",
  name = "",
  updateName = () => {},
  submit = () => {},
  submitText = "SUBMIT",
  history,
  transitionTo
}) => {
  const isEditor = mode === "editor";

  return shopId && !shop ? (
    <div>対象店舗が存在しません。</div>
  ) : (
    <>
      <div style={{ padding: 16 }}>
        <div style={{ marginTop: 16 }}>
          {isEditor ? (
            <TextField label="名前" value={name} onChange={updateName} />
          ) : (
            <>
              <Typography variant="caption" component="label">
                名前
              </Typography>

              <Typography>{shop ? shop.name : ""}</Typography>
            </>
          )}
        </div>

        {isEditor && (
          <div style={{ marginTop: 16 }}>
            <Button
              variant="contained"
              color="primary"
              fullWidth={true}
              onClick={() => {
                submit({
                  id: shopId || "",
                  name
                });
                if (history && transitionTo) {
                  history.push(transitionTo);
                }
              }}
            >
              {submitText}
            </Button>
          </div>
        )}
      </div>
    </>
  );
};

export default Component;
