import * as React from "react";
import {
  compose,
  pure,
  setDisplayName,
  StateHandler,
  StateHandlerMap,
  withStateHandlers
} from "recompose";
import { IShop } from "../../stores/";
import Component, { IRsvShopFormProps } from "./Component";

interface ILocalStateProps {
  name: IShop["name"];
}

type LocalStateHandlerProps = {
  updateName: StateHandler<ILocalStateProps>;
} & StateHandlerMap<ILocalStateProps>;

type IEnhancedProps = IRsvShopFormProps &
  ILocalStateProps &
  LocalStateHandlerProps;

const enhance = compose<IEnhancedProps, IRsvShopFormProps>(
  setDisplayName("EnhancedRsvShopForm"),
  withStateHandlers<
    ILocalStateProps,
    LocalStateHandlerProps,
    IRsvShopFormProps
  >(
    props => {
      return {
        name: props.shop ? props.shop.name : ""
      };
    },
    {
      updateName: state => (e: React.ChangeEvent<HTMLInputElement>) => {
        return {
          ...state,
          name: e.target.value
        };
      }
    }
  ),
  pure
);

export default enhance((Component as unknown) as React.SFC<IEnhancedProps>);
