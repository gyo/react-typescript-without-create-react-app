import Fab, { FabProps } from "@material-ui/core/Fab";
import * as React from "react";
import { Link } from "react-router-dom";

export interface IListItemLinkProps {
  to: string;
  children: React.ReactNode;
  restProps?: any[];
}

const Component: React.SFC<IListItemLinkProps> = ({
  to,
  children,
  ...restProps
}) => {
  const renderLink = (props: FabProps & { children?: React.ReactNode }) => (
    <Link {...props} to={to} innerRef={undefined} />
  );

  return (
    <Fab component={renderLink} {...restProps}>
      {children}
    </Fab>
  );
};

export default Component;
