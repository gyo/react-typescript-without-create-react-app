import ListItem, { ListItemProps } from "@material-ui/core/ListItem";
import * as React from "react";
import { Link } from "react-router-dom";

export interface IListItemLinkProps {
  to: string;
  children: React.ReactNode;
}

const Component: React.SFC<IListItemLinkProps> = ({ to, children }) => {
  const renderLink = (
    props: ListItemProps & { children?: React.ReactNode }
  ) => <Link {...props} to={to} innerRef={undefined} />;

  return (
    <li>
      <ListItem button={true} component={renderLink}>
        {children}
      </ListItem>
    </li>
  );
};

export default Component;
