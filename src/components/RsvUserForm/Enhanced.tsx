import * as React from "react";
import {
  compose,
  pure,
  setDisplayName,
  StateHandler,
  StateHandlerMap,
  withStateHandlers
} from "recompose";
import { IUser } from "../../stores/";
import Component, { IRsvUserFormProps } from "./Component";

interface ILocalStateProps {
  name: IUser["name"];
}

type LocalStateHandlerProps = {
  updateName: StateHandler<ILocalStateProps>;
} & StateHandlerMap<ILocalStateProps>;

type IEnhancedProps = IRsvUserFormProps &
  ILocalStateProps &
  LocalStateHandlerProps;

const enhance = compose<IEnhancedProps, IRsvUserFormProps>(
  setDisplayName("EnhancedRsvUserForm"),
  withStateHandlers<
    ILocalStateProps,
    LocalStateHandlerProps,
    IRsvUserFormProps
  >(
    props => {
      return {
        name: props.user ? props.user.name : ""
      };
    },
    {
      updateName: state => (e: React.ChangeEvent<HTMLInputElement>) => {
        return {
          ...state,
          name: e.target.value
        };
      }
    }
  ),
  pure
);

export default enhance((Component as unknown) as React.SFC<IEnhancedProps>);
