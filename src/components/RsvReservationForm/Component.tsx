import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { History } from "history";
import * as React from "react";
import * as dateTimeLocal from "../../utilities/dateTimeLocal";
import { IMenu } from "../../stores/";
import { IReservation } from "../../stores/";
import { IUser } from "../../stores/";

export interface IRsvReservationFormProps {
  reservationId?: string;
  reservation?: IReservation;
  mode?: "viewer" | "editor";
  menus?: IMenu[];
  users?: IUser[];
  userId?: string;
  updateUserId?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  menuId?: string;
  updateMenuId?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  dateTime?: Date;
  updateDateTime?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  submit?: (reservation: IReservation) => void;
  submitText?: string;
  history?: History;
  transitionTo?: string;
}

const Component: React.SFC<IRsvReservationFormProps> = ({
  reservationId,
  reservation,
  mode = "editor",
  menus = [],
  users = [],
  userId = "",
  updateUserId = () => {},
  menuId = "",
  updateMenuId = () => {},
  dateTime = new Date(),
  updateDateTime = () => {},
  submit = () => {},
  submitText = "SUBMIT",
  history,
  transitionTo
}) => {
  const isEditor = mode === "editor";

  return reservationId && !reservation ? (
    <div>対象予約が存在しません。</div>
  ) : (
    <>
      <div style={{ padding: 16 }}>
        <div style={{ marginTop: 16 }}>
          {isEditor ? (
            <TextField
              select={true}
              label="ユーザーID"
              value={userId}
              onChange={updateUserId}
            >
              {users.map(user => (
                <MenuItem key={user.id} value={user.id}>
                  {user.name}
                </MenuItem>
              ))}
            </TextField>
          ) : (
            <>
              <Typography variant="caption" component="label">
                ユーザーID
              </Typography>

              <Typography>{reservation ? reservation.userId : ""}</Typography>
            </>
          )}
        </div>

        <div style={{ marginTop: 16 }}>
          {isEditor ? (
            <TextField
              select={true}
              label="メニューID"
              value={menuId}
              onChange={updateMenuId}
            >
              {menus.map(menu => (
                <MenuItem key={menu.id} value={menu.id}>
                  {menu.name}
                </MenuItem>
              ))}
            </TextField>
          ) : (
            <>
              <Typography variant="caption" component="label">
                メニューID
              </Typography>

              <Typography>{reservation ? reservation.menuId : ""}</Typography>
            </>
          )}
        </div>

        <div style={{ marginTop: 16 }}>
          {isEditor ? (
            <TextField
              type="datetime-local"
              label="予約日時"
              value={dateTimeLocal.toString(dateTime)}
              onChange={updateDateTime}
            />
          ) : (
            <>
              <Typography variant="caption" component="label">
                予約日時
              </Typography>

              <Typography>
                {reservation
                  ? dateTimeLocal.toString(reservation.dateTime)
                  : dateTimeLocal.toString(new Date())}
              </Typography>
            </>
          )}
        </div>

        {isEditor && (
          <div style={{ marginTop: 16 }}>
            <Button
              variant="contained"
              color="primary"
              fullWidth={true}
              onClick={() => {
                submit({
                  dateTime,
                  id: reservationId || "",
                  menuId,
                  userId
                });
                if (history && transitionTo) {
                  history.push(transitionTo);
                }
              }}
            >
              {submitText}
            </Button>
          </div>
        )}
      </div>
    </>
  );
};

export default Component;
