import * as React from "react";
import {
  compose,
  pure,
  setDisplayName,
  StateHandler,
  StateHandlerMap,
  withStateHandlers
} from "recompose";
import * as dateTimeLocal from "../../utilities/dateTimeLocal";
import { IReservation } from "../../stores/";
import Component, { IRsvReservationFormProps } from "./Component";

interface ILocalStateProps {
  userId: IReservation["userId"];
  menuId: IReservation["menuId"];
  dateTime: IReservation["dateTime"];
}

type LocalStateHandlerProps = {
  updateUserId: StateHandler<ILocalStateProps>;
  updateMenuId: StateHandler<ILocalStateProps>;
  updateDateTime: StateHandler<ILocalStateProps>;
} & StateHandlerMap<ILocalStateProps>;

type IEnhancedProps = IRsvReservationFormProps &
  ILocalStateProps &
  LocalStateHandlerProps;

const enhance = compose<IEnhancedProps, IRsvReservationFormProps>(
  setDisplayName("EnhancedRsvReservationForm"),
  withStateHandlers<
    ILocalStateProps,
    LocalStateHandlerProps,
    IRsvReservationFormProps
  >(
    props => {
      return {
        dateTime: props.reservation ? props.reservation.dateTime : new Date(),
        menuId: props.reservation ? props.reservation.menuId : "",
        userId: props.reservation ? props.reservation.userId : ""
      };
    },
    {
      updateDateTime: state => (e: React.ChangeEvent<HTMLInputElement>) => {
        return {
          ...state,
          dateTime: dateTimeLocal.toDate(e.target.value)
        };
      },
      updateMenuId: state => (e: React.ChangeEvent<HTMLInputElement>) => {
        return {
          ...state,
          menuId: e.target.value
        };
      },
      updateUserId: state => (e: React.ChangeEvent<HTMLInputElement>) => {
        return {
          ...state,
          userId: e.target.value
        };
      }
    }
  ),
  pure
);

export default enhance((Component as unknown) as React.SFC<IEnhancedProps>);
