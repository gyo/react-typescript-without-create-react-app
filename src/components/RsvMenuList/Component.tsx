import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import * as React from "react";
import { IMenu } from "../../stores/";
import ListItemLink from "../ListItemLink";

export interface IRsvMenuCollectionProps {
  menus?: IMenu[];
}

const RsvMenuCollection: React.SFC<IRsvMenuCollectionProps> = ({
  menus = []
}) => (
  <>
    {menus.length === 0 ? (
      <div>メニューが存在しません。</div>
    ) : (
      <List>
        {menus.map(menu => (
          <ListItemLink to={`/rsv-menus/${menu.id}/`} key={menu.id}>
            <ListItemText>{menu.name}</ListItemText>
          </ListItemLink>
        ))}
      </List>
    )}
  </>
);

export default RsvMenuCollection;
