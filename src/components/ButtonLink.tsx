import Button, { ButtonProps } from "@material-ui/core/Button";
import * as React from "react";
import { Link } from "react-router-dom";

export type IButtonLinkProps = ButtonProps & {
  to: string;
};

const Component: React.SFC<IButtonLinkProps> = ({ to, ...restProps }) => {
  const renderLink = (props: ButtonProps & { children?: React.ReactNode }) => (
    <Link {...props} to={to} innerRef={undefined} />
  );

  return <Button component={renderLink} {...restProps} />;
};

export default Component;
