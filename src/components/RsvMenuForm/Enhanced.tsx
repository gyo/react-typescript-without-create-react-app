import * as React from "react";
import {
  compose,
  pure,
  setDisplayName,
  StateHandler,
  StateHandlerMap,
  withStateHandlers
} from "recompose";
import * as dateTimeLocal from "../../utilities/dateTimeLocal";
import { IMenu } from "../../stores/";
import Component, { IRsvMenuFormProps } from "./Component";

interface ILocalStateProps {
  name: IMenu["name"];
  requiredMinutes: IMenu["requiredMinutes"];
  reservableDateTimes: IMenu["reservableDateTimes"];
  resourceOccupancy: IMenu["resourceOccupancy"];
  shopId: IMenu["shopId"];
}

type LocalStateHandlerProps = {
  updateName: StateHandler<ILocalStateProps>;
  updateRequiredMinutes: StateHandler<ILocalStateProps>;
  addReservableDateTime: StateHandler<ILocalStateProps>;
  updateReservableDateTime: StateHandler<ILocalStateProps>;
  deleteReservableDateTime: StateHandler<ILocalStateProps>;
  updateResourceOccupancy: StateHandler<ILocalStateProps>;
  updateShopId: StateHandler<ILocalStateProps>;
} & StateHandlerMap<ILocalStateProps>;

type IEnhancedProps = IRsvMenuFormProps &
  ILocalStateProps &
  LocalStateHandlerProps;

const enhance = compose<IEnhancedProps, IRsvMenuFormProps>(
  setDisplayName("EnhancedRsvMenuForm"),
  withStateHandlers<
    ILocalStateProps,
    LocalStateHandlerProps,
    IRsvMenuFormProps
  >(
    props => {
      return {
        name: props.menu ? props.menu.name : "",
        requiredMinutes: props.menu ? props.menu.requiredMinutes : 0,
        reservableDateTimes: props.menu ? props.menu.reservableDateTimes : [],
        resourceOccupancy: props.menu ? props.menu.resourceOccupancy : 0,
        shopId: props.menu ? props.menu.shopId : ""
      };
    },
    {
      addReservableDateTime: state => () => {
        return {
          ...state,
          reservableDateTimes: [
            ...state.reservableDateTimes,
            {
              endTime: new Date(),
              startTime: new Date()
            }
          ]
        };
      },
      deleteReservableDateTime: state => (listIndex: number) => {
        return {
          ...state,
          reservableDateTimes: [
            ...state.reservableDateTimes.slice(0, listIndex),
            ...state.reservableDateTimes.slice(listIndex + 1)
          ]
        };
      },
      updateName: state => (e: React.ChangeEvent<HTMLInputElement>) => {
        return {
          ...state,
          name: e.target.value
        };
      },
      updateRequiredMinutes: state => (
        e: React.ChangeEvent<HTMLInputElement>
      ) => {
        const requiredMinutes = parseInt(e.target.value, 10);
        return {
          ...state,
          requiredMinutes: isNaN(requiredMinutes) ? 0 : requiredMinutes
        };
      },
      updateReservableDateTime: state => (
        e: React.ChangeEvent<HTMLInputElement>,
        listIndex: number,
        startOrEnd: "start" | "end"
      ) => {
        return {
          ...state,
          reservableDateTimes: [
            ...state.reservableDateTimes.slice(0, listIndex),
            {
              ...state.reservableDateTimes[listIndex],
              [`${startOrEnd}Time`]: dateTimeLocal.toDate(e.target.value)
            },
            ...state.reservableDateTimes.slice(listIndex + 1)
          ]
        };
      },
      updateResourceOccupancy: state => (
        e: React.ChangeEvent<HTMLInputElement>
      ) => {
        const resourceOccupancy = parseInt(e.target.value, 10);
        return {
          ...state,
          resourceOccupancy: isNaN(resourceOccupancy) ? 0 : resourceOccupancy
        };
      },
      updateShopId: state => (e: React.ChangeEvent<HTMLInputElement>) => {
        return {
          ...state,
          shopId: e.target.value
        };
      }
    }
  ),
  pure
);

export default enhance((Component as unknown) as React.SFC<IEnhancedProps>);
