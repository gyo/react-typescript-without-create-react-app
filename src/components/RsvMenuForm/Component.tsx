import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { History } from "history";
import * as React from "react";
import { IMenu } from "../../stores/";
import { IShop } from "../../stores/";
import ReservableDateTimeList, {
  IRsvReservableDateTimeListProps
} from "./ReservableDateTimeList";

export type IRsvMenuFormProps = IRsvReservableDateTimeListProps & {
  menuId?: string;
  menu?: IMenu;
  shops?: IShop[];
  mode?: "viewer" | "editor";
  name?: string;
  updateName?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  requiredMinutes?: number;
  updateRequiredMinutes?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  resourceOccupancy?: number;
  updateResourceOccupancy?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  shopId?: string;
  updateShopId?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  submit?: (menu: IMenu) => void;
  submitText?: string;
  history?: History;
  transitionTo?: string;
};

const Component: React.SFC<IRsvMenuFormProps> = ({
  menuId,
  menu,
  mode = "editor",
  shops = [],
  name = "",
  updateName = () => {},
  requiredMinutes = 0,
  updateRequiredMinutes = () => {},
  reservableDateTimes = [],
  addReservableDateTime = () => {},
  updateReservableDateTime = () => () => {},
  deleteReservableDateTime = () => {},
  resourceOccupancy = 0,
  updateResourceOccupancy = () => {},
  shopId = "",
  updateShopId = () => {},
  submit = () => {},
  submitText = "SUBMIT",
  history,
  transitionTo
}) => {
  const isEditor = mode === "editor";

  return menuId && !menu ? (
    <div>対象メニューが存在しません。</div>
  ) : (
    <>
      <div style={{ padding: 16 }}>
        <div style={{ marginTop: 16 }}>
          {isEditor ? (
            <TextField label="名前" value={name} onChange={updateName} />
          ) : (
            <>
              <Typography variant="caption" component="label">
                名前
              </Typography>

              <Typography>{menu ? menu.name : ""}</Typography>
            </>
          )}
        </div>

        <div style={{ marginTop: 16 }}>
          {isEditor ? (
            <TextField
              label="所要時間"
              value={requiredMinutes}
              onChange={updateRequiredMinutes}
            />
          ) : (
            <>
              <Typography variant="caption" component="label">
                所要時間
              </Typography>

              <Typography>{menu ? menu.requiredMinutes : ""}</Typography>
            </>
          )}
        </div>

        <div style={{ marginTop: 16 }}>
          <Typography variant="caption" component="label">
            予約可能日時
          </Typography>

          <div>
            <ReservableDateTimeList
              mode={isEditor ? "editor" : "viewer"}
              reservableDateTimes={
                isEditor
                  ? reservableDateTimes
                  : menu
                  ? menu.reservableDateTimes
                  : []
              }
              addReservableDateTime={addReservableDateTime}
              updateReservableDateTime={updateReservableDateTime}
              deleteReservableDateTime={deleteReservableDateTime}
            />
          </div>
        </div>

        <div style={{ marginTop: 16 }}>
          {isEditor ? (
            <TextField
              label="リソース占有率"
              value={resourceOccupancy}
              onChange={updateResourceOccupancy}
            />
          ) : (
            <>
              <Typography variant="caption" component="label">
                リソース占有率
              </Typography>

              <Typography>{menu ? menu.resourceOccupancy : ""}</Typography>
            </>
          )}
        </div>

        <div style={{ marginTop: 16 }}>
          {isEditor ? (
            <TextField
              select={true}
              label="店舗ID"
              value={shopId}
              onChange={updateShopId}
            >
              {shops.map(shop => (
                <MenuItem key={shop.id} value={shop.id}>
                  {shop.name}
                </MenuItem>
              ))}
            </TextField>
          ) : (
            <>
              <Typography variant="caption" component="label">
                店舗ID
              </Typography>

              <Typography>{menu ? menu.shopId : ""}</Typography>
            </>
          )}
        </div>

        {isEditor && (
          <div style={{ marginTop: 16 }}>
            <Button
              variant="contained"
              color="primary"
              fullWidth={true}
              onClick={() => {
                submit({
                  id: menuId || "",
                  name,
                  requiredMinutes,
                  reservableDateTimes,
                  resourceOccupancy,
                  shopId
                });
                if (history && transitionTo) {
                  history.push(transitionTo);
                }
              }}
            >
              {submitText}
            </Button>
          </div>
        )}
      </div>
    </>
  );
};

export default Component;
