import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import * as React from "react";
import { IShop } from "../../stores/";
import ListItemLink from "../ListItemLink";

export interface IRsvShopCollectionProps {
  shops?: IShop[];
}

const RsvShopCollection: React.SFC<IRsvShopCollectionProps> = ({
  shops = []
}) => (
  <>
    {shops.length === 0 ? (
      <div>店舗が存在しません。</div>
    ) : (
      <List>
        {shops.map(shop => (
          <ListItemLink to={`/rsv-shops/${shop.id}/`} key={shop.id}>
            <ListItemText>{shop.name}</ListItemText>
          </ListItemLink>
        ))}
      </List>
    )}
  </>
);

export default RsvShopCollection;
