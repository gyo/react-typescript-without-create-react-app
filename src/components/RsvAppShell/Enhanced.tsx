import * as React from "react";
import {
  compose,
  pure,
  setDisplayName,
  StateHandler,
  StateHandlerMap,
  withStateHandlers
} from "recompose";
import Component, { IRsvAppShellProps } from "./Component";

interface ILocalStateProps {
  drawerIsOpen: boolean;
}

type LocalStateHandlerProps = {
  onCloseDrawer: StateHandler<ILocalStateProps>;
  onOpenDrawer: StateHandler<ILocalStateProps>;
} & StateHandlerMap<ILocalStateProps>;

type IEnhancedProps = IRsvAppShellProps &
  ILocalStateProps &
  LocalStateHandlerProps;

const enhance = compose<IEnhancedProps, IRsvAppShellProps>(
  setDisplayName("EnhancedRsvAppShell"),
  withStateHandlers<
    ILocalStateProps,
    LocalStateHandlerProps,
    IRsvAppShellProps
  >(
    props => {
      return {
        drawerIsOpen: props.drawerIsOpen ? true : false
      };
    },
    {
      onCloseDrawer: state => () => {
        return {
          ...state,
          drawerIsOpen: false
        };
      },
      onOpenDrawer: state => () => {
        return {
          ...state,
          drawerIsOpen: true
        };
      }
    }
  ),
  pure
);

export default enhance((Component as unknown) as React.SFC<IEnhancedProps>);
