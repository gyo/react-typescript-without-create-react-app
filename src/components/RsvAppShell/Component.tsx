import AppBar from "@material-ui/core/AppBar";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem, { ListItemProps } from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import MenuIcon from "@material-ui/icons/Menu";
import * as React from "react";
import { Link } from "react-router-dom";

interface IListItemLinkProps {
  primary: string;
  to: string;
  pathname: string;
}

const ListItemLink: React.SFC<IListItemLinkProps> = ({
  primary,
  to,
  pathname
}) => {
  const renderLink = (
    props: ListItemProps & { children?: React.ReactNode }
  ) => <Link {...props} to={to} innerRef={undefined} />;

  return (
    <li>
      <ListItem
        button={true}
        component={renderLink}
        selected={pathname.startsWith(to)}
      >
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  );
};

export interface IRsvAppShellProps {
  title: string;
  pathname: string;
  drawerIsOpen?: boolean;
  onCloseDrawer?: () => void;
  onOpenDrawer?: () => void;
}

const Component: React.SFC<IRsvAppShellProps> = ({
  title,
  pathname,
  drawerIsOpen = false,
  onCloseDrawer = () => {},
  onOpenDrawer = () => {},
  children
}) => {
  const toggleDrawer = () => {
    if (drawerIsOpen) {
      onCloseDrawer();
    } else {
      onOpenDrawer();
    }
  };

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <IconButton color="inherit" onClick={toggleDrawer}>
            <MenuIcon />
          </IconButton>

          <Typography color="inherit" variant="h6" component="h1">
            {title}
          </Typography>
        </Toolbar>
      </AppBar>

      <SwipeableDrawer
        open={drawerIsOpen}
        onClose={onCloseDrawer}
        onOpen={onOpenDrawer}
      >
        <div>
          <IconButton onClick={onCloseDrawer}>
            <ChevronLeftIcon />
          </IconButton>
        </div>

        <Divider />

        <List component="nav">
          <ListItemLink primary="トップ" to="/rsv-home/" pathname={pathname} />
          <ListItemLink
            primary="予約一覧"
            to="/rsv-reservations/"
            pathname={pathname}
          />
          <ListItemLink
            primary="店舗一覧"
            to="/rsv-shops/"
            pathname={pathname}
          />
          <ListItemLink
            primary="メニュー一覧"
            to="/rsv-menus/"
            pathname={pathname}
          />
          <ListItemLink
            primary="ユーザー一覧"
            to="/rsv-users/"
            pathname={pathname}
          />
        </List>
      </SwipeableDrawer>

      {children}
    </>
  );
};

export default Component;
