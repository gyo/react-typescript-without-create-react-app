export interface IShop {
  id: string;
  name: string;
}

export const shops = [
  {
    id: "shopId0",
    name: "店舗0"
  },
  {
    id: "shopId1",
    name: "店舗1"
  },
  {
    id: "shopId2",
    name: "店舗2"
  },
  {
    id: "shopId3",
    name: "店舗3"
  },
  {
    id: "shopId4",
    name: "店舗4"
  }
];
