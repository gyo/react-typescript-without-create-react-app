import { menus, IMenu } from "./menus";
import { reservations, IReservation } from "./reservations";
import { shops, IShop } from "./shops";
import { users, IUser } from "./users";

export interface IStore {
  menus: IMenu[];
  reservations: IReservation[];
  shops: IShop[];
  users: IUser[];
}

export const initialState: IStore = {
  menus,
  reservations,
  shops,
  users
};

export * from "./menus";
export * from "./reservations";
export * from "./shops";
export * from "./users";
