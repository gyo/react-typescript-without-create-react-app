export interface IReservation {
  id: string;
  userId: string;
  menuId: string;
  dateTime: Date;
}

export const reservations = [
  {
    dateTime: new Date("2019-01-01 09:00"),
    id: "reservationId0",
    menuId: "menuId0",
    userId: "userId0"
  },
  {
    dateTime: new Date("2019-01-01 10:00"),
    id: "reservationId1",
    menuId: "menuId0",
    userId: "userId1"
  },
  {
    dateTime: new Date("2019-01-01 11:00"),
    id: "reservationId2",
    menuId: "menuId0",
    userId: "userId2"
  },
  {
    dateTime: new Date("2019-01-01 13:00"),
    id: "reservationId3",
    menuId: "menuId0",
    userId: "userId3"
  },
  {
    dateTime: new Date("2019-01-01 14:00"),
    id: "reservationId4",
    menuId: "menuId0",
    userId: "userId4"
  },
  {
    dateTime: new Date("2019-01-01 13:00"),
    id: "reservationId5",
    menuId: "menuId1",
    userId: "userId0"
  },
  {
    dateTime: new Date("2019-01-01 13:40"),
    id: "reservationId6",
    menuId: "menuId1",
    userId: "userId0"
  },
  {
    dateTime: new Date("2019-01-01 14:20"),
    id: "reservationId7",
    menuId: "menuId1",
    userId: "userId0"
  },
  {
    dateTime: new Date("2019-01-01 15:00"),
    id: "reservationId8",
    menuId: "menuId1",
    userId: "userId0"
  },
  {
    dateTime: new Date("2019-01-04 13:10"),
    id: "reservationId9",
    menuId: "menuId8",
    userId: "userId2"
  },
  {
    dateTime: new Date("2019-01-04 13:30"),
    id: "reservationId10",
    menuId: "menuId8",
    userId: "userId2"
  },
  {
    dateTime: new Date("2019-01-04 13:50"),
    id: "reservationId11",
    menuId: "menuId8",
    userId: "userId2"
  },
  {
    dateTime: new Date("2019-01-04 14:10"),
    id: "reservationId12",
    menuId: "menuId8",
    userId: "userId2"
  },
  {
    dateTime: new Date("2019-01-04 14:30"),
    id: "reservationId13",
    menuId: "menuId8",
    userId: "userId2"
  },
  {
    dateTime: new Date("2019-01-04 14:50"),
    id: "reservationId14",
    menuId: "menuId8",
    userId: "userId2"
  }
];
